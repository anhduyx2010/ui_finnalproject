import HomePage from "../Pages/HomePage";
import BlogList from "../Pages/BlogList";
import Login from "../Pages/Login";
import Register from "../Pages/Register";

const ROUTERS = [
    {
        name: "HomePage",
        path: "/",
        exact: true,
        Component: HomePage,
    },
    {
        name: "BlogList",
        path: "/blog",
        exact: false,
        Component: BlogList,
    },
    {
        name: "Login",
        path: "/login",
        exact: false,
        Component: Login,
    },
    {
        name: "Register",
        path: "/register",
        exact: false,
        Component: Register,
    },
];
export default ROUTERS;
