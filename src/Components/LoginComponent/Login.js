import React from "react";
import { useDispatch } from "react-redux";

import { useHistory } from "react-router-dom";

import { Form, Input, Button, Checkbox } from "antd";

import { LoginContainer, LoginTitle } from "./Login.styled";
import { asyncHandleLogin } from "../../stores/Auth/actions";
import { useNotAuth } from "../../helper";

function LoginForm() {
    useNotAuth();
    const dispatch = useDispatch();
    const history = useHistory();
    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 8 },
    };

    const tailLayout = {
        wrapperCol: { offset: 8, span: 8 },
    };

    const onFinish = (value) => {
        dispatch(asyncHandleLogin(value))
            .then((res) => {
                if (res.message === "Login Oke") {
                    history.push("/");
                } else {
                    history.push("/login");
                }
            })
            .catch((error) => {
                history.push("/login");
                console.log(error);
            });
    };

    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };
    return (
        <LoginContainer>
            <LoginTitle>Login Form</LoginTitle>
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        { required: true, message: "Please input your email!" },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Please input your password!",
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    {...tailLayout}
                    name="remember"
                    valuePropName="checked"
                >
                    <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </LoginContainer>
    );
}

export default LoginForm;
