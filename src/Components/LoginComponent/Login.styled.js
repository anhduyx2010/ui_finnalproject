import styled from 'styled-components'

export const LoginContainer = styled.div`
    margin: 20px 0;
`

export const LoginTitle = styled.h2`
    text-align: center
`