import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";

import { Layout, Menu } from "antd";

import { ROUTERS } from "../../constants";

function HeaderComponent() {
    const [curentUrl, setCurentUrl] = useState("/");
    const history = useHistory();
    const location = useLocation();

    useEffect(() => {
        setCurentUrl(location.pathname);
    }, [location]);

    function onHandleSetCurrentUrl(selectedCureentUrl) {
        setCurentUrl(selectedCureentUrl.key);
        history.push(selectedCureentUrl.key);
    }

    return (
        <Layout>
            <Menu
                theme="light"
                mode="horizontal"
                onClick={onHandleSetCurrentUrl}
                selectedKeys={[curentUrl]}
            >
                {ROUTERS.map((menuItem) => {
                    return (
                        <Menu.Item key={menuItem.path}>
                            {menuItem.name}
                        </Menu.Item>
                    );
                })}
            </Menu>
        </Layout>
    );
}

export default HeaderComponent;
