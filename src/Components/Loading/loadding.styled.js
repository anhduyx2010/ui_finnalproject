import styled from "styled-components";

export const SvgStyledLoadingContainer = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;

    overflow: hidden;

    background: rgba(0, 0, 0, 0.5);

    ${(props) => {
        if (props.isLoading) {
            return `
                opacity: 1;
                visibility: visible;
                z-index: 1000;
                display: block
            `;
        } else {
            return `
            opacity: 0;
            visibility: hidden;
            z-index: 0
            `;
        }
    }}
`;

export const SvgStyled = styled.svg`
    background: transparent;

    display: block;

    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;

    width: 200px;
    height: 200px;
`;
