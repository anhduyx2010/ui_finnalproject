import { useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

export default function useNotAuth() {
    const history = useHistory();
    const location = useLocation();
    const token = useSelector((state) => state.Auth.ACCESS_TOKEN);
    useEffect(() => {
        if (token) {
            history.push("/");
        }
    }, [location, token, history]);
}
