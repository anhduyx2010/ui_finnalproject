import React from "react";
import { Switch, Route } from "react-router-dom";
import { ROUTERS } from "./constants";
import BlogDetail from "./Pages/BlogDetail";

export default function Router() {
    return (
        <div>
            <Switch>
                {ROUTERS.map((route, index) => {
                    return (
                        <Route
                            key={index}
                            exact={route.exact}
                            path={route.path}
                        >
                            <route.Component />
                        </Route>
                    );
                })}
            </Switch>
            <Switch>
                <Route path="/blog/:_id" children={<BlogDetail/>} />
            </Switch>
        </div>
    );
}
