import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { asyncHandleGetBlogPostWithId } from "../stores/Blog/actions";

import { Layout } from 'antd';
// import {DeleteOutlined, EditOutlined} from '@ant-design/icons'; 
const { Content } = Layout;

function BlogDetail() {
    const dispatch = useDispatch();
    let { _id } = useParams();
    useEffect(() => {
        dispatch(asyncHandleGetBlogPostWithId({ _id }));
    }, [dispatch, _id]);
    const blogPostId = useSelector((state) => state.Blog.data_id);
    console.log(blogPostId)
    return (
        <Layout>
            <Content>
                <div>
                    <h3>{blogPostId.title}</h3>
                    <p>{blogPostId.content}</p>
                </div>
            </Content>
        </Layout>
    );
}

export default BlogDetail;
