import React, { useEffect } from "react";

import { Menu } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { Layout } from 'antd';

import { useDispatch, useSelector } from "react-redux";

import { asyncHandleGetBlogPost } from "../stores/Blog/actions";
import { Link } from "react-router-dom";

function BlogList() {
    const { Sider } = Layout;
    const { SubMenu } = Menu;
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(asyncHandleGetBlogPost());
    }, [dispatch]);

    const blogPost = useSelector((state) => state.Blog.data);
    return (
        <Sider>
            <Menu style={{ width: 200, float:"left" }} mode="vertical">
                <SubMenu key="sub1" icon={<MailOutlined />} title="Blog Post">
                    <Menu.ItemGroup title="Story">
                        {blogPost.map((blog, index) => {
                            return (
                                <Menu.Item key={index}>
                                    <Link to={`/blog/${blog._id}`}>{blog.title}</Link>
                                </Menu.Item>
                            )
                        })}
                    </Menu.ItemGroup>
                </SubMenu>
            </Menu>
        </Sider>
    );
}

export default BlogList;
