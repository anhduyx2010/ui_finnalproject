import axios from "axios";

const BASE_URL = 'https://duytran-api-blogposts.herokuapp.com/'

const api = {
    call() {
        return axios.create({
            baseURL : BASE_URL,
            headers: {
                'Content-type' : 'application/json'
            }
        })
    },

    callWithAuth() {
        return axios.create({
            baseURL: BASE_URL,
            headers: {
                'Content-type' : 'application/json',
                'Authetication': 'asclkasklcjaskljd'
            }
        })
    }
}

export default api