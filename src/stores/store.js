import { createStore, applyMiddleware } from "redux";
import rootReducer from "./root-reducer";

import logger from 'redux-logger'

import ReduxThunk from "redux-thunk";
const store = createStore(rootReducer, applyMiddleware(ReduxThunk, logger));

export default store;
