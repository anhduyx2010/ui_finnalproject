import { notification } from "antd";

import axios from "axios";

import { actHidenLoading, actShowLoading } from "../App/actions";
import { Storage } from "../../helper";

export const LOGIN_SUCCESS = "LOGIN_SUCCESS";

export const actLoginSuccess = ({ tokenKey }) => {
    return {
        type: LOGIN_SUCCESS,
        payload: {
            tokenKey,
        },
    };
};

export const asyncHandleLogin = ({ email, password }) => {
    const openNotification = (message) => {
        notification.open({
            message: message,
            style: {
                width: 600,
                marginLeft: 335 - 600,
            },
        });
    };
    return async (dispatch) => {
        try {
            dispatch(actShowLoading());
            const res = await axios.post(
                "https://duytran-api-blogposts.herokuapp.com/api/login",
                {
                    email,
                    password,
                }
            );
            if (res.status !== 200) {
                openNotification(res.data.message);
                dispatch(actHidenLoading());
                return {
                    message: res.data.error,
                };
            } else {
                openNotification(res.data.message);
                Storage.setToken(res.data.tokenKey);
                dispatch(actLoginSuccess(res.data));
                dispatch(actHidenLoading());
                return {
                    message: res.data.message,
                };
            }
        } catch (error) {
            dispatch(actHidenLoading());
            return {
                message: error,
            };
        }
    };
};
