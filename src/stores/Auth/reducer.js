import { LOGIN_SUCCESS } from "./actions";
import { Storage } from "../../helper";

const initState = {
    ACCESS_TOKEN: Storage.getToken(),
};

export default function AuthReducer(state = initState, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                ACCESS_TOKEN: action.payload.tokenKey,
            };
        default:
            return state;
    }
}
