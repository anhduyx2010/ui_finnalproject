import { combineReducers } from "redux";

import AuthReducer from "./Auth/reducer";
import UserReducer from "./Users/reducer";
import AppReducer from "./App/reducer";
import BlogReducer from "./Blog/reducer";

const rootReducer = combineReducers({
    Auth: AuthReducer,
    User: UserReducer,
    App: AppReducer,
    Blog: BlogReducer,
});

export default rootReducer;
