import Axios from "axios";
import { notification } from "antd";

import { actHidenLoading, actShowLoading } from "../App/actions";

export const SET_USER_INFO = "SET_USER_INFO";
export const REGISTER = "REGISTER";

export const actSetUserInfo = (user) => {
    return {
        type: SET_USER_INFO,
        payload: user,
    };
};

export const actReGister = ({ name, email, password }) => {
    console.log("actReGister:", name, email, password);
    return {
        type: REGISTER,
        payload: {
            name,
            email,
            password,
        },
    };
};

export const asyncHandleRegisterForm = ({ name, email, password }) => {
    const openNotification = (message) => {
        notification.open({
            message: message,
            style: {
                width: 600,
                marginLeft: 335 - 600,
            },
        });
    };
    return async (dispatch) => {
        try {
            dispatch(actShowLoading());
            const res = await Axios.post(
                "https://duytran-api-blogposts.herokuapp.com/api",
                {
                    name,
                    email,
                    password,
                }
            );
            console.log(res);
            if (res.status === 200) {
                if (res.data.result === "Failed") {
                    dispatch(actHidenLoading());
                    openNotification(res.data.message);
                } else {
                    actHidenLoading();
                    dispatch(actReGister({ name, email, password }));
                    openNotification(res.data.message);
                }
            } else {
                dispatch(actHidenLoading());
                openNotification(res.data.message);
            }
        } catch (error) {}
    };
};
