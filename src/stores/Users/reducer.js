import { SET_USER_INFO, REGISTER } from "./actions";

const initState = {
    currentUser: null,
    register: [],
};

export default function UsersReducer(state = initState, action) {
    switch (action.type) {
        case SET_USER_INFO:
            return {
                ...state,
                currentUser: action.payload.user,
            };
        case REGISTER:
            return {
                ...state,
                register: action.payload.register,
            };
        default:
            return state;
    }
}
