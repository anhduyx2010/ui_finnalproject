import { GET_BLOG, GET_BLOG_ID } from "./actions";
const initState = {
    data: [],
    data_id: [],
};

export default function BlogReducer(state = initState, action) {
    switch (action.type) {
        case GET_BLOG:
            return {
                ...state,
                data: action.payload,
            };
        case GET_BLOG_ID:
            return {
                ...state,
                data_id: action.payload,
            };

        default:
            return state;
    }
}
