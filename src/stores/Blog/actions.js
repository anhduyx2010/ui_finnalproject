import Axios from "axios";

import { actHidenLoading, actShowLoading } from "../App/actions";

export const GET_BLOG = "GET_BLOG";
export const GET_BLOG_ID = "GET_BLOG_ID";

export function actGetBlog(data) {
    return {
        type: GET_BLOG,
        payload: data,
    };
}

export function actGetBlogId(data_id) {
    return {
        type: GET_BLOG_ID,
        payload: data_id,
    };
}

export function asyncHandleGetBlogPost() {
    return async (dispatch) => {
        try {
            dispatch(actShowLoading());
            const res = await Axios.get(
                "https://duytran-api-blogposts.herokuapp.com/blog"
            );
            dispatch(actGetBlog(res.data.data));
            dispatch(actHidenLoading());
        } catch (error) {
            console.log(error);
        }
    };
}

export function asyncHandleGetBlogPostWithId({ _id }) {
    return async (dispatch) => {
        try {
            dispatch(actShowLoading());
            const res = await Axios.get(
                `https://duytran-api-blogposts.herokuapp.com/blog/${_id}`
            );
            dispatch(actGetBlogId(res.data.data));
            dispatch(actHidenLoading());
        } catch (error) {
            console.log(error);
        }
    };
}
