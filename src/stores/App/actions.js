export const SHOW_LOADING = 'SHOW LOADING';
export const HIDDEN_LOADING = 'HIDDEN LOADING';

export const actShowLoading = () => {
    return {
        type: SHOW_LOADING,
        payload: null
    }
}

export const actHidenLoading = () => {
    return {
        type: HIDDEN_LOADING,
        payload: null
    }
}