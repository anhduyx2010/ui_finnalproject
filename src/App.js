import React from "react";
import { useSelector } from "react-redux";

// import api from './API/apiService'
import HeaderComponent from "./Components/HeaderComponent/HeaderComponent";
import Router from "./router";
import Loading from "./Components/Loading/Loading";

export default function App() {
    const isLoading = useSelector((state) => state.App.isLoading);
    return (
        <div>
            <HeaderComponent />
            <Router />
            <Loading isLoading={isLoading} />
        </div>
    );
}
